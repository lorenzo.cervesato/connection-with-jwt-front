describe('Test appFront', () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit('http://localhost:3000/');
  })

  it('We are in Home Login AND Display two fields by default', () => {
    cy.get('h2').should('have.text', 'Home Login');
    cy.get('.loginform label input').should('have.length', 2);
    cy.get('.loginform label').first().should('have.text', ' Email : ');
    cy.get('.loginform label').last().should('have.text', ' Password : ');
  })

  it('We are in Home Login, Can Login with a specific user, AND Modify his data', () => {
    const login = 'lorenzo.cervesato@gmail.com';
    const password = '1234';

    const newlogin = 'lorenzo.cervesato2@gmail.com';
    const newpassword = '12345';

    // On se log avec les identifiants entrés en base.
    cy.get('h2').should('have.text', 'Home Login');
    cy.get('.loginform label input').first().type(`${login}`);
    cy.get('.loginform label input').last().type(`${password}`);
    cy.get('.loginform input[type="submit"]').click();
    // On est loggé
    cy.get('h2').should('have.text', 'Home Logged');
    cy.get('.updateForm label').first().should('have.text', ' New email : ');
    cy.get('.updateForm label').last().should('have.text', ' New password : ');
    // On entre les modifications
    cy.get('.updateForm label input').first().type(`${newlogin}`);
    cy.get('.updateForm label input').last().type(`${newpassword}`);
    cy.get('.updateForm input[type="submit"]').click();
    // Ceci nous délog
    cy.get('h2').should('have.text', 'Home Login');
    cy.get('.loginform label').first().should('have.text', ' Email : ');
    cy.get('.loginform label').last().should('have.text', ' Password : ');
    // On entre les nouveaux identifiants
    cy.get('.loginform label input').first().type(`${newlogin}`);
    cy.get('.loginform label input').last().type(`${newpassword}`);
    cy.get('.loginform input[type="submit"]').click();
    // On est loggé avec les nouveaux identifiants
    cy.get('h2').should('have.text', 'Home Logged');
    cy.get('.updateForm label').first().should('have.text', ' New email : ');
    cy.get('.updateForm label').last().should('have.text', ' New password : ');
    // On entre les modifications pour réinitialiser les données en base
    cy.get('.updateForm label input').first().type(`${login}`);
    cy.get('.updateForm label input').last().type(`${password}`);
    cy.get('.updateForm input[type="submit"]').click();
    // Ceci nous délog à nouveau, prêt à relancer le test
    cy.get('h2').should('have.text', 'Home Login');
    cy.get('.loginform label').first().should('have.text', ' Email : ');
    cy.get('.loginform label').last().should('have.text', ' Password : ');

  })
})
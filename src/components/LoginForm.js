import React, {useState, useEffect} from 'react';
import axios from 'axios';

const LoginForm = (props) => {
    const [emailVal,setEmailVal] = useState("");
    const [pswVal,setPswVal] = useState("");
    const [submit, setSubmit] = useState(0);

    useEffect(() => {
        if(submit === 1){
            axios.post("http://localhost:3001/api/v1/utilisateurs/login", {
                email: emailVal,
                password: pswVal
            })
            .then(function (response) {
                props.setUserid(response.data.id);
                props.setLogged(response.data.login);
                props.setToken(response.data.token);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
  
        return setSubmit(0);
    },[submit]);

    // Redondance de code dans les 2 formulaires.
    const handleSubmit = (event) => {
        setSubmit(submit+1);
        event.preventDefault();
    }
    // Redondance de code dans les 2 formulaires.
    const handleChange = (event) => {
        switch (event.target.type) {
            case 'text':
                setEmailVal(event.target.value);
                break;
            case 'password':
                setPswVal(event.target.value);
                break;
            default:
                console.log('ERROR');
                break;
        }
    }

    return <>
        <form className="loginform" onSubmit={handleSubmit}>
            <label> Email : <input type="text" value={emailVal} onChange={handleChange} /></label>
            <label> Password : <input type="password" value={pswVal} onChange={handleChange} /></label>
            <input type="submit" value="Envoyer" />
        </form>
    </>;
}

export default LoginForm;
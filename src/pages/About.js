import React from 'react';
import { Card } from 'react-bootstrap';

const About = () => {
    return <Card variant="outlined" style={{backgroundColor:'rgb(58, 58, 58)', width: '80%', margin: 'auto', padding: '20px'}}>
        <h2>ABOUT</h2>
        <p>Made by Laurent Cervesato, Developper Web and Mobile.</p>
        <p>Email: lorenzo.cervesato@gmail.com</p>
    </Card>;
}

export default About;
import React, {useState} from 'react';
import { Card } from 'react-bootstrap';
import LoginForm from '../components/LoginForm';
import UpdateForm from '../components/UpdateForm';

const Home = () => {
    const [logged,setLogged] = useState(false);
    const [token,setToken] = useState("");
    const [userid,setUserid] = useState(null);

    return <Card variant="outlined" style={{backgroundColor:'rgb(58, 58, 58)', width: '80%', margin: 'auto', padding: '20px'}}>
        <h2>Home {logged?'Logged':'Login'}</h2>
        {!logged?<LoginForm setUserid={setUserid} setToken={setToken} setLogged={setLogged}/>:<UpdateForm userid={userid} token={token} setLogged={setLogged}/>}
    </Card>;
}

export default Home;